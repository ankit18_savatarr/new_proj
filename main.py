from flask import Flask, request
import json
from sql_operations import insert
app = Flask(__name__)

@app.route("/data",methods=["POST"])
def save():
    device_id = request.form.get("device_id")
    latitude = request.form.get("latitude")
    longitude = request.form.get("longitude")
    interest_data = request.form.get("interest_data")
    print("device id: ", device_id)
    print("latitude: ", latitude)
    print("longitude: ", longitude)
    print("insert_data: ", interest_data)
    query = "INSERT INTO api_tae (device_id, latitude, longitude, interest_data) VALUES ('{}','{}','{}','{}')".format(device_id, latitude, longitude, interest_data)
    result = insert(query)
    print(result)
    return json.dumps({'code': 200, 'message': 'Data Added Successfully !!'})

if __name__ == "__main__":
    # app.run(host='0.0.0.0', debug=True)
    app.run(debug=True)
